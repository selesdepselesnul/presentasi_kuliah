<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BookController@getIndex')->name('index');
Route::post('/user/login', array('uses'=>'UserController@postLogin'));
Route::get('/user/logout', array('uses'=>'UserController@getLogout'));
Route::get('/cart', array('before'=>'auth.basic','as'=>'cart','uses'=>'CartController@getIndex'));
Route::post('/cart/add', array('before'=>'auth.
basic','uses'=>'CartController@postAddToCart'));
Route::get('/cart/delete/{id}',
array('before'=>'auth.basic','as'=>'delete_book_from_cart','uses'=>'CartController@getDelete'));

Route::post('/order', [
  'before'=>'auth.basic',
  'uses'=>'OrderController@postOrder'
]);
Route::get('/user/orders', [
  'before'=>'auth.basic',
  'uses'=>'OrderController@getIndex'
]);
