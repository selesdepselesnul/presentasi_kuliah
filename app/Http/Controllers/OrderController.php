<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request as Request;
use Illuminate\Support\Facades\Auth;
use App\Cart as Cart;
use App\Order as Order;
use App\Book as Book;


class OrderController extends Controller
{
    public function postOrder(Request $request)
    {
        $rules=[
          'address'=>'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {
            return redirect()->route('cart')->with('error','Address fieldis required!');
        }
        $member_id = Auth::user()->id;
        $address = $request->get('address');

        $cart_books = Cart::with('Book')->where('member_id','=',$member_id)->get();
        $cart_total = Cart::with('Book')->where('member_id','=',$member_id)->sum('total');
        
        if(count($cart_books) == 0)
            return redirect()->route('index')->with('error','Your cart is empty.');


        $order = Order::create([
          'member_id'=>$member_id,
          'address'=>$address,
          'total'=>$cart_total
        ]);
        foreach ($cart_books as $order_books) {
            $order->orderItems()->attach($order_books->book_id, [
                'amount'=>$order_books->amount,
                'price'=>$order_books->book->price,
                'total'=>$order_books->book->price*$order_books->amount
            ]);
        }
        Cart::where('member_id','=',$member_id)->delete();
        return redirect()->route('index')->with('message','Your order processed successfully.');
    }

    public function getIndex(){
        $member_id = Auth::user()->id;
        if(Auth::user()->admin) {
          $orders=Order::all();
        } else {
          $orders=Order::with('orderItems')->where('member_id','=',$member_id)->get();
        }
        if(!$orders){
          return Redirect::route('index')->with('error','There is no order.');
        }
        return view('order')
                    ->with('orders', $orders);
    }
}
