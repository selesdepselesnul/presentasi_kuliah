<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function postLogin(Request $request)
    {
        $email=$request->get('email');
        $password=$request->get('password');
        if (Auth::attempt(array('email' => $email, 'password' =>
            $password)))
        {
            return redirect()->route('index');
        } else {
            return redirect()->route('index')
                    ->with('error','Please check your password & email');
        }
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('index');
    }
}
