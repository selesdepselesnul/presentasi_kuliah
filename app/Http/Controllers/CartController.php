<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cart as Cart;
use App\Book as Book;

class CartController extends Controller
{
  public function postAddToCart(Request $request)
  {
      $rules=array(
        'amount'=>'required|numeric',
        'book'=>'required|numeric|exists:books,id'
      );

      $validator = Validator::make($request->all(), $rules);

      if ($validator->fails()) {
          return redirect()->route('index')->with('error','The book could not added to your cart!');
      } else if(is_null(Auth::user())) {
          return redirect()
                    ->route('index')
                    ->with(
                      'error',
                      'The book could not added to your cart, please login first!');
      } else {
          $member_id = Auth::user()->id;
          $book_id = $request->get('book');
          $amount = $request->get('amount');
          $book = Book::find($book_id);
          $total = $amount * $book->price;
          $count = Cart::where('book_id','=',$book_id)->where('member_id','=',$member_id)->count();
          if($count){
            return redirect()->route('index')->with('error','The book already in your cart.');
          }
          Cart::create(
            array(
              'member_id'=>$member_id,
              'book_id'=>$book_id,
              'amount'=>$amount,
              'total'=>$total
            )
          );
          return redirect()->route('cart');
      }
  }

  public function getIndex()
  {
    if (is_null(Auth::user())) {
        return redirect()
                ->route('index')
                ->with('error','Your cart is empty, please login first!');
    } else {
        $member_id = Auth::user()->id;
        $cart_books=Cart::with('Book')->where('member_id','=',$member_id)->get();
        $cart_total=Cart::with('Book')->where('member_id','=',$member_id)->sum('total');

        if(count($cart_books) == 0){
            return redirect()->route('index')->with('error','Your cart is empty');
        }
        return view('cart')
                    ->with('cart_books', $cart_books)
                    ->with('cart_total',$cart_total);
    }
  }


  public function getDelete($id){
      $cart = Cart::find($id)->delete();
      return redirect()->route('cart');
  }
}
