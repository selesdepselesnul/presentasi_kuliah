<?php

use App\Book;
use Illuminate\Database\Seeder;

Class BooksTableSeeder extends Seeder {
    public function run()
    {
        DB::table('books')->delete();
        Book::create(array(
          'title'=>'Requiem',
          'isbn'=>'9780062014535',
          'price'=>'13.40',
          'cover'=>'1.jpg',
          'author_id'=>1
        ));
        Book::create(array(
          'title'=>'Twilight',
          'isbn'=>'9780316015844',
          'price'=>'15.40',
          'cover'=>'2.jpg',
          'author_id'=>2
        ));
        Book::create(array(
          'title'=>'Deception Point',
          'isbn'=>'9780671027384',
          'price'=>'16.40',
          'cover'=>'3.jpg',
          'author_id'=>3
        ));
    }
}
